#!/bin/sh

# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2009-2014 Stephan Raue (stephan@openelec.tv)
# Copyright (C) 2016-present Team LibreELEC (https://libreelec.tv)
. /etc/profile
oe_setup_addon chrome.youtube

# check if chrome is already successful installed
#echo "teste"
#echo $LGagent
if [ ! -f "$CHROME_DIR/extract.ok" ]; then
  cd $CHROME_DIR
  chrome-downloader
fi

if [ -e $ADDON_HOME/env ]
then
  . $ADDON_HOME/env
fi
# make sure we use "own" gtk/pango/nss/etc
export LD_LIBRARY_PATH=$CHROME_DIR/lib

# configure pango/pixbuf
export PANGO_RC_FILE=$CHROME_DIR/config/pangorc
export GDK_PIXBUF_MODULE_FILE=$CHROME_DIR/config/pixbuf.loaders.cache

# font rendering in gtk widgets is brokeen with nvidia blob. use our Xdefaults
export XENVIRONMENT=$CHROME_DIR/config/Xdefaults

# start unclutter
if [ "$HIDE_CURSOR" == "true" ]
then
  unclutter &
  UNCLUTTER_PID=$!
fi

# User Agent
case $USERAGENT in 
  'LGTV') UA="\"Mozilla/5.0 (Web0S; Linux/SmartTV) AppleWebKit/537.36 (KHTML, like Gecko) QtWebEngine/5.2.1 Chrome/38.0.2125.122 Safari/537.36 LG Browser/8.00.00(LGE; 60UH6550-UB; 03.00.15; 1; DTV_W16N); webOS.TV-2016; LG NetCast.TV-2013 Compatible (LGE, 60UH6550-UB, wireless)\"";;
  'Chrome') UA="";;
  'Roku') UA="\"Mozilla/5.0 (compatible; U; NETFLIX) AppleWebKit/533.3 (KHTML, like Gecko) Qt/4.7.0 Safari/533.3 Netflix/3.2 (DEVTYPE=RKU-42XXX-; CERTVER=0) QtWebKit/2.2, Roku 3/7.0 (Roku, 4200X, Wireless)\"";;
  'SamsungTV') UA="\"Mozilla/5.0 (Linux; Tizen 2.3; SmartHub; SMART-TV; SmartTV; U; Maple2012) AppleWebKit/538.1+ (KHTML, like Gecko) TV Safari/538.1+\"";;
  'SonyBravia') UA="\"Mozilla/5.0 (Linux; BRAVIA 4K 2015 Build/LMY48E.S265) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36 OPR/28.0.1754.0\"";;
  'AppleTV') UA="\"AppleTV6,2/11.1\"";;
esac

# vaapi
LIBVA_DRIVERS_PATH="/usr/lib/dri:$CHROME_DIR/lib"
LIBVA_DRIVER_NAME=''
case $VAAPI_MODE in
  'intel')
      LIBVA_DRIVER_NAME='i965'
      chrome_OPTS="$chrome_OPTS --enable-accelerated-video"
      ;;
  'amd')
      LIBVA_DRIVER_NAME='vdpau'
      chrome_OPTS="$chrome_OPTS --enable-accelerated-video"
      ;;
  'nvidia')
      LIBVA_DRIVER_NAME='vdpau'
      chrome_OPTS="$chrome_OPTS --enable-accelerated-video --allow-no-sandbox-job --disable-gpu-sandbox"
      ;;
  *)
      LIBGL_ALWAYS_SOFTWARE='1'
      export LIBGL_ALWAYS_SOFTWARE
esac
export LIBVA_DRIVER_NAME LIBVA_DRIVERS_PATH

# windowed
case $WINDOW_MODE in
  'fullscreen') chrome_OPTS="$chrome_OPTS --start-fullscreen";;
  'kiosk') chrome_OPTS="$chrome_OPTS --kiosk";;
esac

# rasterization
case $RASTER_MODE in
  'off') chrome_OPTS="$chrome_OPTS --disable-accelerated-2d-canvas --disable-gpu-compositing";;
  'force') chrome_OPTS="$chrome_OPTS --enable-gpu-rasterization --enable-accelerated-2d-canvas --ignore-gpu-blacklist";;
esac

# alsa
if [ ! -z $ALSA_DEVICE ]; then
  chrome_OPTS="$chrome_OPTS --alsa-output-device=$ALSA_DEVICE"
fi

# dark mode
if [ "$DARK_MODE" == "true" ]
then
  chrome_OPTS="$chrome_OPTS --force-dark-mode"
fi

# HACK!!! to get sound at Chrome stop pulseaudio
systemctl stop pulseaudio

# start chrome
LD_PRELOAD=/usr/lib/libGL.so $CHROME_DIR/chrome-bin/chrome \
  --app-id=lmlbegeinfagimckgkpmkpkkcojdkplf \
  --args \
  --user-agent="'$UA'" \
  $chrome_OPTS \
  --no-sandbox \
  --user-data-dir=$ADDON_HOME/profile \
  --test-type $@ \
  2>&1 | tee $ADDON_LOG_FILE

# kill unclutter
if [ "$HIDE_CURSOR" == "true" ]
then
  kill $UNCLUTTER_PID
fi

# HACK!!! to get sound at Kodi start pulseaudio
sleep 5
systemctl start pulseaudio
